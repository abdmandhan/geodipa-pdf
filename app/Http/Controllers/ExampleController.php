<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Support\Facades\App;
use Illuminate\Support\Facades\View;
use Illuminate\Http\Request;


class ExampleController extends Controller
{
    public $DAYS = [
        ["key" => "Sunday", "name" => "minggu"],
        ["key" => "Monday", "name" => "senin"],
        ["key" => "Tuesday", "name" => "selasa"],
        ["key" => "Wednesday", "name" => "rabu"],
        ["key" => "Thursday", "name" => "kamis"],
        ["key" => "Friday", "name" => "jumat"],
        ["key" => "Saturday", "name" => "sabtu"],
    ];

    public $DAYS_NUMBER = [
        ["key" => "1", "name" => "satu"],
        ["key" => "2", "name" => "dua"],
        ["key" => "3", "name" => "tiga"],
        ["key" => "4", "name" => "empat"],
        ["key" => "5", "name" => "lima"],
        ["key" => "6", "name" => "enam"],
        ["key" => "7", "name" => "tujuh"],
        ["key" => "8", "name" => "delapan"],
        ["key" => "9", "name" => "sembilan"],
        ["key" => "10", "name" => "sepuluh"],
        ["key" => "11", "name" => "sebelas"],
        ["key" => "12", "name" => "dua belas"],
        ["key" => "13", "name" => "tiga belas"],
        ["key" => "14", "name" => "empat belas"],
        ["key" => "15", "name" => "lima belas"],
        ["key" => "16", "name" => "enam belas"],
        ["key" => "17", "name" => "tujuh belas"],
        ["key" => "18", "name" => "delapan belas"],
        ["key" => "19", "name" => "sembilan belas"],
        ["key" => "20", "name" => "dua puluh"],
        ["key" => "21", "name" => "dua puluh satu"],
        ["key" => "22", "name" => "dua puluh dua"],
        ["key" => "23", "name" => "dua puluh tiga"],
        ["key" => "24", "name" => "dua puluh empat"],
        ["key" => "25", "name" => "dua puluh lima"],
        ["key" => "26", "name" => "dua puluh enam"],
        ["key" => "27", "name" => "dua puluh tujuh"],
        ["key" => "28", "name" => "dua puluh delapan"],
        ["key" => "29", "name" => "dua puluh sembilan"],
        ["key" => "30", "name" => "tiga puluh"],
        ["key" => "31", "name" => "tiga puluh satu"],
    ];

    public $MONTHS = [
        ["key" => "1", "name" => "januari"],
        ["key" => "2", "name" => "februari"],
        ["key" => "3", "name" => "maret"],
        ["key" => "4", "name" => "april"],
        ["key" => "5", "name" => "mei"],
        ["key" => "6", "name" => "juni"],
        ["key" => "7", "name" => "juli"],
        ["key" => "8", "name" => "agustus"],
        ["key" => "9", "name" => "september"],
        ["key" => "10", "name" => "oktober"],
        ["key" => "11", "name" => "nopember"],
        ["key" => "12", "name" => "desember"],
    ];
    public $day_idn, $month_idn, $year_idn, $date_idn, $date_formatted, $date_bast;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function generate_bapp(Request $request)
    {
        try {

            $data = $this->validate($request, [
                'date'              => ['required', 'date_format:d-m-Y'],
                'task_id'           => ['required'],
                'number'            => ['required'],
                'review_text'       => ['required'],
                'contract_no'       => ['required'],
                'contract_name'     => ['required'],
                'party_1_name'      => ['required'],
                'party_1_position'  => ['required'],
                'party_2_name'      => ['required'],
                'party_2_position'  => ['required'],
                'vendor_name'       => ['required'],
                'addendum_no'       => ['array'],
            ]);

            $this->generateDate($data['date']);
            // return [$day_idn, $month_idn, $year_idn, $date_idn, $date_formatted];
            $addendum_no = $data['addendum_no'] ?? [];

            $layout = View::make(
                'bapp',
                [
                    'task_id' => $data['task_id'],
                    'number' => $data['number'],
                    'review_text' => $data['review_text'],
                    'contract_no' => $data['contract_no'],
                    'contract_name' => $data['contract_name'],
                    'party_1_name' => $data['party_1_name'],
                    'party_1_position' => $data['party_1_position'],
                    'party_2_name' => $data['party_2_name'],
                    'party_2_position' => $data['party_2_position'],
                    'vendor_name' => $data['vendor_name'],
                    //DATE
                    'day_idn'   => $this->day_idn,
                    'date_idn' => $this->date_idn,
                    'month_idn' => $this->month_idn,
                    'year_idn' => $this->year_idn,
                    'date'  => $this->date_formatted,
                    'addendum_no' => count($addendum_no) == 0 ? null : $addendum_no
                ]
            );

            // return $layout;

            /* Create PDF */
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($layout->render());
            $url = "news/BAPP_" . $data['task_id'] . '.pdf';
            $pdf->save(base_path('public/' . $url));
        } catch (\Throwable $th) {
            return $th;
            // throw $th;
        }

        if (env('APP_ENV') == 'dev') {
            return url("$url");
        } else {
            return url("v1/$url");
        }

        // return $pdf->stream();
    }

    public function generate_bast(Request $request)
    {
        $data = $this->validate($request, [
            'date'              => ['required', 'date_format:d-m-Y'],
            'task_id'           => ['required'],
            'number'            => ['required'],
            'contract_no'       => ['required'],
            'contract_name'     => ['required'],
            'contract_date'     => ['required'],
            'party_1_name'      => ['required'],
            'party_1_position'  => ['required'],
            'party_2_name'      => ['required'],
            'party_2_position'  => ['required'],
            'vendor_name'       => ['required'],
            'termin_name'       => ['required'],
            'bapp'              => ['required', 'array'],
        ]);

        $this->generateDate($data['date'], $data['contract_date']);
        // return [$day_idn, $month_idn, $year_idn, $date_idn, $date_formatted];

        $layout = View::make(
            'bast',
            [
                'task_id' => $data['task_id'],
                'number' => $data['number'],
                'contract_no' => $data['contract_no'],
                'contract_name' => $data['contract_name'],
                'contract_date' => $data['contract_date'],
                'party_1_name' => $data['party_1_name'],
                'party_1_position' => $data['party_1_position'],
                'party_2_name' => $data['party_2_name'],
                'party_2_position' => $data['party_2_position'],
                'vendor_name' => $data['vendor_name'],
                'termin_name' => $data['termin_name'],
                'bapp' => $data['bapp'],
                //DATE
                'day_idn'   => $this->day_idn,
                'date_idn' => $this->date_idn,
                'month_idn' => $this->month_idn,
                'year_idn' => $this->year_idn,
                'date'  => $this->date_formatted,
                'date_bast' => $this->date_bast,
            ]
        );
        /* Create PDF */
        try {
            $pdf = App::make('dompdf.wrapper');
            $pdf->loadHTML($layout->render());
            $url = "news/BAST_" . $data['task_id'] . '.pdf';
            $pdf->save(base_path('public/' . $url));
        } catch (\Throwable $th) {
            return $th;
        }

        // return url("$url");
        if (env('APP_ENV') == 'dev') {
            return url("$url");
        } else {
            return url("v1/$url");
        }
        // return $pdf->stream();
    }

    private function generateDate($date, $date_bast = null)
    {
        $date = Carbon::create($date);
        $this->day_idn = collect($this->DAYS)->where('key', $date->format('l'))->first()['name'];
        $this->month_idn = collect($this->MONTHS)->where('key', $date->format('m'))->first()['name'];

        $this->year_idn = collect($this->DAYS_NUMBER)->where('key', $date->format('y'))->first()['name'];
        $this->date_idn = collect($this->DAYS_NUMBER)->where('key', $date->format('d'))->first()['name'];

        $this->date_formatted = $date->format('d-m-Y');
        if ($date_bast) {
            $date_bast = Carbon::create($date_bast);
            $this->date_bast = $date_bast->format('d-m-Y');
        }
    }
}
