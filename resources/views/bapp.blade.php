<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>TITLE</title>
    <style>
        .h1 {
            font-weight: bold;
        }

        .text-center {
            text-align: center;
        }

        .bold {
            font-weight: bold;
        }

        body {
            font-size: 16px;
        }

        .header {
            margin-bottom: 40px;
        }

        .party {
            margin-top: 40px;
        }

        .remarks {
            margin-top: 40px;
        }

        .uppercase {
            text-transform: uppercase;
        }
    </style>
</head>

<body>
    <!-- header -->
    <div class="header">
        <table width="100%">
            <tr class="h1 text-center">
                <td>BERITA ACARA PENYELESAIAN PEKERJAAN (BAPP)</td>
            </tr>
            <tr class="h1 text-center uppercase">
                <td>{{ $contract_name }}</td>
            </tr>
            <tr class="text-center">
                <td>{{ $number }}</td>
            </tr>
        </table>
    </div>

    <!-- main -->
    <div class="main">
        <div>
            Pada hari ini, {{ $day_idn }} tanggal {{ $date_idn }} bulan {{ $month_idn }} tahun dua ribu
            {{ $year_idn }}
            &#40;{{ $date }}&#41;,
        </div>
        <div>kami yang bertanda tangan dibawah ini:</div>
        <div class="party">
            <table>
                <!-- pihak pertama -->
                <tr>
                    <td>1.</td>
                    <td>Nama</td>
                    <td>:</td>
                    <td class="bold">{{ $party_1_name }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td class="bold">{{ $party_1_position }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Selanjutnya disebut</td>
                    <td>:</td>
                    <td class="bold">PIHAK PERTAMA</td>
                </tr>
            </table>

            <table style="margin-top: 20px">
                <!-- pihak kedua -->

                <tr>
                    <td>2.</td>
                    <td>Nama</td>
                    <td>:</td>
                    <td class="bold">{{ $party_2_name }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Jabatan</td>
                    <td>:</td>
                    <td class="bold">{{ $party_2_position }}</td>
                </tr>
                <tr>
                    <td></td>
                    <td>Selanjutnya disebut</td>
                    <td>:</td>
                    <td class="bold">PIHAK KEDUA</td>
                </tr>
            </table>
        </div>

        <div class="remarks">
            <p>
                PIHAK PERTAMA Telah melaksanakan pemeriksaan pekerjaan
                sebagai berikut:
            </p>

            <table>
                <tr>
                    <td>Jenis pekerjaan</td>
                    <td>:</td>
                    <td>{{ $contract_name }}</td>
                </tr>
                <tr>
                    <td>Pelaksana pekerjaan</td>
                    <td>:</td>
                    <td>{{ $vendor_name }}</td>
                </tr>
                <tr>
                    <td>Dasar pelaksanaan pekerjaan</td>
                    <td>:</td>
                    @if ($addendum_no == null)
                        <td>Perjanjian Nomor: {{ $contract_no }}</td>
                    @else
                        <td>Perjanjian Nomor:
                            @foreach ($addendum_no as $item)
                                {{ $item }}
                                @if (!$loop->last)
                                    ,
                                @endif
                            @endforeach
                        </td>
                    @endif
                </tr>
                <tr>
                    <td>Hasil pekerjaan yang telah diselesaikan</td>
                    <td>:</td>
                    <td>{{ $review_text }}</td>
                </tr>
            </table>

            <p>
                Demikian Berita Acara Penyelesaian Pekerjaan ini dibuat dalam
                rangkap 2 (dua) untuk dapat dipergunakan sebagaimana
                mestinya.
            </p>
        </div>
        <br><br>
        <div style="text-align: center; position:relative">
            <div style="position: absolute">
                <div style="position: relative; height:250px">
                    <div>PIHAK PERTAMA,</div>
                    <div>PT. GEO DIPA ENERGI (PERSERO)</div>
                    <div style="bottom:0; position: absolute; width:100%">{{ $party_1_name }}</div>
                </div>
            </div>
            <div style="position: absolute; right:0; width:300px">
                <div style="position: relative; height:250px">
                    <div>PIHAK KEDUA,</div>
                    <div>{{ $vendor_name }}</div>
                    <div style="bottom:0; position: absolute; width:100%">{{ $party_2_name }}</div>
                </div>
            </div>
        </div>
    </div>
</body>

</html>
