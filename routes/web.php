<?php

/** @var \Laravel\Lumen\Routing\Router $router */


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

if (env('APP_ENV') == 'dev') {
    $prefix = "";
} else {
    $prefix = "v2/document";
}

$router->get("$prefix", function () use ($router) {
    return $router->app->version() . " | Service for generate BAPP and BAST | version 1.0.0 | application env " . env('APP_ENV');
});

$router->post("$prefix/generate-bapp", "ExampleController@generate_bapp");
$router->get("$prefix/bapp", "ExampleController@generate_bapp");
$router->post("$prefix/generate-bast", "ExampleController@generate_bast");
